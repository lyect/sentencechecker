import nltk
import re
f = open("./sentence_rules.txt", "r")
grammar = f.read()
grammar1 = nltk.data.load('grammars/large_grammars/atis.cfg')
big_g = str(grammar1)[56:] + "\n" + grammar

# All parts of speech which are included in the dictionary
# Uncomment it if you want to check this out
"""
for rule in grammar.split("\n"):
    splitted_rule = rule.split("|")
    print(splitted_rule[0] + ": " + ", ".join(splitted_rule[1:3]))
"""

nltk.grammar._STANDARD_NONTERM_RE = re.compile('( [\w/][\w$/^<>-]* ) \s*', re.VERBOSE)

# Some simple grammar
big_g = """
S -> NP | VP | NP VP
NP -> NN | DT NN | DT NNS | PRP$ NN | PRP$ NNS | JJ NN | JJ NNS | JJS NN | JJS NNS | JJR NN | JJR NNS | RB NN | RB NNS | NN NP
VP -> VBP VBG | VB NP | VP PP | VBP JJ | VBZ NN | VBP NNS""" + "\n" + big_g

rules = nltk.CFG.fromstring(big_g)
