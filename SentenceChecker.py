import nltk
import Rules


class Analyzer:
    def __init__(self, parser=nltk.parse.ChartParser, rules=Rules.rules, response=True):
        self._response = response
        self.rules = rules
        self.parser = parser(rules)

    def check(self, sentence):
        correct = False
        sentence = sentence.lower().split()
        try:
            for x in self.parser.parse(sentence):
                if x:
                    correct = True
            if correct:
                if self._response:
                    print("I think this sentence is correct")
                return 1
            else:
                if self._response:
                    print("I think this sentence is not correct")
                return 0
        except ValueError:
            print("Input contains unknown words. Try to change your rules")
