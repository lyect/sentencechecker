**This analyzer checks correctness of a sentence**

* Current features:
  * simple grammar
  * all words are taken from oxford dictionary
    (thanks to dwyl for his work)
* Features to be added in the future
  * ML algorithms which will give an opportunity to check sentences not only by their syntax but by the meaning
  * Advanced grammar
